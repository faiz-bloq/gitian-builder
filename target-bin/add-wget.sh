#!/bin/sh

# Get an installed package manifest

mkdir -p /var/lib/buildroot/temp && rm -rf /var/lib/buildroot/temp/*
cd /var/lib/buildroot/temp 
wget https://ftp.gnu.org/gnu/wget/wget-1.17.tar.gz && tar xvf wget-1.17.tar.gz 
apt-get install -y libcurl4-gnutls-dev
cd /var/lib/buildroot/temp/wget-1.17
./configure && make install && wget --version
rm /usr/bin/wget
ln -s /usr/local/bin/wget /usr/bin/wget
echo "Updated Wget." >> ~/wgetout
wget --version >> ~/wgetout
